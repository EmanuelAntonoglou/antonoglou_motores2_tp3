using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateCamera : MonoBehaviour
{
    public Transform objetoOrbitar;
    public float distancia = 10f;
    public float velocidadHorizontal = 100f;
    public float velocidadVertical = 50f;
    public float limiteVerticalSuperior = 80f; // L�mite superior de rotaci�n vertical
    public float limiteVerticalInferior = -80f; // L�mite inferior de rotaci�n vertical

    private float rotacionVerticalActual = 0f; // Mantener un registro de la rotaci�n vertical actual

    void Start()
    {
        if (objetoOrbitar == null)
        {
            objetoOrbitar = transform.parent;
        }

        ActualizarPosicion();
    }

    void Update()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float rotacionHorizontal = horizontal * velocidadHorizontal * Time.deltaTime;

        float vertical = Input.GetAxis("Vertical");
        float rotacionVertical = vertical * velocidadVertical * Time.deltaTime;

        transform.RotateAround(objetoOrbitar.position, Vector3.down, rotacionHorizontal);

        // Verificar si estamos dentro de los l�mites de rotaci�n vertical
        if (rotacionVertical != 0f)
        {
            if ((rotacionVertical > 0f && rotacionVerticalActual < limiteVerticalSuperior) ||
                (rotacionVertical < 0f && rotacionVerticalActual > limiteVerticalInferior))
            {
                transform.RotateAround(-objetoOrbitar.position, transform.right, rotacionVertical);
                rotacionVerticalActual += rotacionVertical;
            }
        }

        ActualizarPosicion();
    }

    void ActualizarPosicion()
    {
        Vector3 posicion = objetoOrbitar.position - transform.forward * distancia;
        transform.position = posicion;
        transform.LookAt(objetoOrbitar);
    }
}
