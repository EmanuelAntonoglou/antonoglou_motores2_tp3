using System.Collections;
using System.Collections.Generic;
using UnityEditor.Timeline.Actions;
using UnityEngine;

public class StackableObject : MonoBehaviour
{
    public bool inTower = false;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Stackable" && collision.gameObject.GetComponent<StackableObject>() != null)
        {
            if (!inTower && collision.gameObject.GetComponent<StackableObject>().inTower)
            {
                inTower = true;
                TowerController.i.stackableObjects.Add(GetComponent<Collider>());
            }
        }
    }

}
