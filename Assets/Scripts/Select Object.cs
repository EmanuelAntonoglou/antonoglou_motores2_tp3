using UnityEngine;

public class SelectObject : MonoBehaviour
{
    [SerializeField] private Material matOutline;
    private Material[] originalMaterials;
    private bool isSelected = false;
    private Vector3 mouseOffset;

    private Rigidbody rb;
    private bool wasGravityEnabled; // Variable to store the state of gravity
    [SerializeField] private float scrollSpeed;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
        originalMaterials = GetComponent<Renderer>().materials;
    }

    private void OnMouseEnter()
    {
        AddOutlineMaterial();
    }

    private void OnMouseExit()
    {
        if (!isSelected) // Restore only if not currently selected
            GetComponent<Renderer>().materials = originalMaterials;
    }

    private void OnMouseDown()
    {
        isSelected = true;
        rb.velocity = Vector3.zero;
        mouseOffset = Input.mousePosition - Camera.main.WorldToScreenPoint(transform.position);

        // Store gravity state on click
        wasGravityEnabled = rb.useGravity;
        rb.useGravity = false;

        // Freeze rotation constraints
        rb.constraints = RigidbodyConstraints.FreezeRotation;

        // Apply outline material
        AddOutlineMaterial();
    }

    private void OnMouseDrag()
    {
        if (isSelected)
        {
            // Get mouse position in world space
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition - mouseOffset);

            // Maintain current Y position, move in X and Z
            mousePos.y = transform.position.y;

            // Move object on X and Z axes
            rb.MovePosition(mousePos);
        }
    }

    private void OnMouseUp()
    {
        if (isSelected)
        {
            isSelected = false;

            // Restore gravity when object is released
            rb.useGravity = wasGravityEnabled;

            // Restore original constraints
            rb.constraints = RigidbodyConstraints.None;

            // Restore original material
            GetComponent<Renderer>().materials = originalMaterials;
        }
    }

    private void Update()
    {
        // Control Y-axis movement with mouse wheel if selected
        if (isSelected)
        {
            float scroll = Input.GetAxis("Mouse ScrollWheel") * scrollSpeed;
            if (scroll != 0f)
            {
                Vector3 newPosition = transform.position + Vector3.up * scroll;
                rb.MovePosition(newPosition);
            }
        }
    }

    private void AddOutlineMaterial()
    {
        if (originalMaterials.Length > 1)
        {
            Material[] newMaterials = (Material[])originalMaterials.Clone();
            newMaterials[1] = matOutline;
            GetComponent<Renderer>().materials = newMaterials;
        }
    }
}
