using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using TMPro;
using Unity.VisualScripting;
using UnityEditor.Timeline.Actions;

public class TowerController : MonoBehaviour
{
    public Transform objetoOrbitar;
    [SerializeField] private TextMeshProUGUI TMPHeight;
    [NonSerialized] public static TowerController i;
    public float totalHeight = 0f;
    public List<Collider> stackableObjects = new List<Collider>();
    private bool canStackObject = true;

    private void Awake()
    {
        CrearSingleton();
    }

    public void CrearSingleton()
    {
        if (i == null)
        {
            i = this;
        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    void Update()
    {
        CalculateTowerHeight();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Stackable")
        {
            if (canStackObject && !collision.gameObject.GetComponent<StackableObject>().inTower)
            {
                canStackObject = false;
                collision.gameObject.GetComponent<StackableObject>().inTower = true;
                stackableObjects.Add(collision.gameObject.GetComponent<Collider>());
            }
        }
    }

    private void CalculateTowerHeight()
    {
        totalHeight = 0f;
        TMPHeight.transform.position = Vector3.zero;
        foreach (Collider col in stackableObjects) 
        {
            totalHeight += col.bounds.size.y;
        }
        TMPHeight.text = (Math.Round(totalHeight, 1).ToString() + "cm");
        ActualizarPosicion();
    }

    void ActualizarPosicion()
    {
        Vector3 posicion = new Vector3();
        if (totalHeight == 0)
        {
            posicion = new Vector3(objetoOrbitar.position.x - 2, 1.0f, objetoOrbitar.position.z);
        }
        else
        {
            posicion = new Vector3(objetoOrbitar.position.x - 2, totalHeight + 0.5f, objetoOrbitar.position.z);
        }
        TMPHeight.transform.position += posicion;

        // Mirror the text by inverting local scale on the X-axis
        TMPHeight.transform.localScale = new Vector3(TMPHeight.transform.localScale.x, TMPHeight.transform.localScale.y, TMPHeight.transform.localScale.z);
    }


}
