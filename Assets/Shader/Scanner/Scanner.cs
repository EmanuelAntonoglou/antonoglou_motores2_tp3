using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scanner : MonoBehaviour
{
    [Header("Scanner Data")]
    [SerializeField] private float speed;
    [SerializeField] private float destroyTime;


    void Start()
    {
        DestroyObjetc();
    }

    void Update()
    {
        ScaleObject();
    }

    private void DestroyObjetc()
    {
        Destroy(gameObject, destroyTime);
    }

    private void ScaleObject()
    {
        Vector3 vectorMesh = transform.localScale;
        float growing = speed * Time.deltaTime;
        transform.localScale += new Vector3(growing, growing, growing);
    }
}
